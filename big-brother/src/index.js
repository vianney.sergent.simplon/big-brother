import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; // on importe un style globale pour la page
import App from './containers/App/App';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
   <App/>
  </React.StrictMode>,
  document.getElementById('root')
);
