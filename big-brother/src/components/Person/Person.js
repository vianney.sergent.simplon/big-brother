import React, { useState } from 'react';
import './Person.css';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/Card';
import AdditionalInfo from './../AdditionalInfo/AdditionalInfo.js';




export default function Person({personData}) {
    const [showModal, setshowModal] = useState(false)
    
    const handleClick = () => {
        setshowModal(!showModal);
    };
    
    return (
        <React.Fragment>
        <CardColumns> 
            <Card style={{ width: '13rem'}} >
                <Card.Img variant="top" src={personData.picture} alt="person's picture" onClick={handleClick} />
                <Card.Body>
                    <Card.Title> {personData.name.first} {personData.name.last}</Card.Title>
                    <Card.Text>
                        {personData.about}
                    </Card.Text>
                </Card.Body>
            </Card>
            
        </CardColumns>
        <AdditionalInfo personData={personData} show={showModal} handleClick={handleClick} />
        </React.Fragment>
        );
};
