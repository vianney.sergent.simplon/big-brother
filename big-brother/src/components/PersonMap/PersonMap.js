import React, { useState } from 'react';
import { Map as LeafletMap, TileLayer, Marker } from 'react-leaflet'

export default function PersonMap({positions}) {

    return (
      <LeafletMap className="truc" center={positions[0]} zoom={12}>
        <TileLayer attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.osm.org/{z}/{x}/{y}.png' />
        {positions.map(position => <Marker position={position}></Marker>)}
      </LeafletMap>
    );
}