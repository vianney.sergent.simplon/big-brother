import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from '../../containers/Home';
import data from './../../data/data.json'


function modifyDatas(personData) {
    const positions = personData.positions
    const positionArray = positions.map(position => {
        const positionString = position.split(', ')
        const newPosition = [parseFloat(positionString[0]), parseFloat(positionString[1])]
        return newPosition
    });
    return positionArray;
}

const myDatas = data.map(personData => {
    const newPositions = modifyDatas(personData);
    const newPersonData = personData
    newPersonData.positions = newPositions
    return newPersonData;
});

const App = () => { 

    return(
        <main>
            <Home myDatas={myDatas} />
        </main>
    );

};

export default App;