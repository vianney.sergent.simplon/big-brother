import Person from './../../components/Person/Person.js'
import SearchBar from './../../components/SearchBar/SearchBar.js'
import './Home.css';
import React, {useState, useEffect} from 'react';


export default function Home({myDatas}) {
    const [searchValue, setSearchValue] = useState('');
    const [searchData, setSearchData] = useState(myDatas)

    useEffect(() => {
        const newData = myDatas.filter(data => {
            let fullName = data.name.first + ' ' + data.name.last
            fullName = fullName.toLowerCase();
            return fullName.includes(searchValue);
        })
        setSearchData(newData);
    }, [searchValue]);

    const handleChange = (event) => {
        setSearchValue(event.target.value);
    }

    const names = myDatas.map(personData => {
        const name = personData.name.first + ' ' + personData.name.last
        return name
    })

    return (
    <React.Fragment>
        <SearchBar names={names} handleChange={handleChange} />
        {searchData.map((personData) => <Person personData={personData} key={personData._id}/>)}
    </React.Fragment>
    );
}